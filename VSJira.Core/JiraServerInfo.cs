﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class JiraServerInfo
    {
        public JiraServerInfo()
        {
        }

        public JiraServerInfo(string name, string url, string username)
        {
            this.Name = name;
            this.Url = url;
            this.Username = username;
        }

        [DisplayName("Server Name")]
        [Description("The friendly name to referece this server.")]
        public string Name { get; set; }

        [DisplayName("Server Url")]
        [Description("The fully qualified URL of this server.")]
        public string Url { get; set; }

        [DisplayName("User Name")]
        [Description("The username to use to authenticate with this server.")]
        public string Username { get; set; }
    }
}
