﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraIssueUserControlViewModel : ViewModelBase
    {
        private JiraIssueViewModel _originalIssue;
        private JiraIssueViewModel _editableIssue;
        private bool _isIdle = true;
        private JiraCommentsPagerViewModel _commentsPager;
        private string _newComment;
        private CancellationTokenSource _attachmentsCancellationSource;
        private CancellationTokenSource _worklogsCancellationSource;

        public VSJiraServices Services { get; private set; }
        public ObservableCollection<string> Priorities { get; private set; }
        public ObservableCollection<string> Statuses { get; private set; }
        public ObservableCollection<string> Resolutions { get; private set; }
        public ObservableCollection<string> Types { get; private set; }
        public ObservableCollection<AttachmentViewModel> Attachments { get; private set; }
        public ObservableCollection<Worklog> Worklogs { get; private set; }
        public DelegateCommand SaveIssueCommand { get; private set; }
        public DelegateCommand RefreshIssueCommand { get; private set; }
        public DelegateCommand AddCommentCommand { get; private set; }
        public DelegateCommand TransitionIssueCommand { get; private set; }
        public event EventHandler Initialized;

        public JiraIssueUserControlViewModel()
        {
            this._attachmentsCancellationSource = new CancellationTokenSource();
            this._worklogsCancellationSource = new CancellationTokenSource();

            this.Priorities = new ObservableCollection<string>();
            this.Statuses = new ObservableCollection<string>();
            this.Resolutions = new ObservableCollection<string>();
            this.Types = new ObservableCollection<string>();
            this.Attachments = new ObservableCollection<AttachmentViewModel>();
            this.Worklogs = new ObservableCollection<Worklog>();

            this.RefreshIssueCommand = new DelegateCommand(
                async () => await RefreshIssueAsync(),
                () => this.IsIdle && this.Issue != null);

            this.TransitionIssueCommand = new DelegateCommand(
                async () => await TransitionIssueAsync(),
                () => this.IsIdle && this.Issue != null);

            this.AddCommentCommand = new DelegateCommand(
                async () => await AddCommentAsync(),
                () => this.IsIdle && this.Issue != null);

            this.SaveIssueCommand = new DelegateCommand(
                async () => await SaveIssueAsync(),
                () => this.IsIdle && this.Issue != null);
        }

        public void Initialize(JiraIssueViewModel issue, VSJiraServices services)
        {
            this.Issue = issue;
            this.Services = services;
            OnInitialized();
        }

        public JiraCommentsPagerViewModel CommentsPager
        {
            get { return _commentsPager; }
            set
            {
                _commentsPager = value;
                OnPropertyChanged("CommentsPager");
            }
        }

        public string NewComment
        {
            get { return _newComment; }
            set
            {
                _newComment = value;
                OnPropertyChanged("NewComment");
            }
        }

        public JiraIssueViewModel Issue
        {
            get { return _editableIssue; }
            private set
            {
                _originalIssue = value;
                _editableIssue = _originalIssue.Clone();
                OnPropertyChanged("Issue");
                this.LoadComboBoxes();
            }
        }

        public bool IsIdle
        {
            get { return _isIdle; }
            set
            {
                _isIdle = value;
                OnPropertyChanged("IsIdle");
            }
        }

        public async Task LoadWorklogsAsync()
        {
            this._worklogsCancellationSource.Cancel();
            this._worklogsCancellationSource = new CancellationTokenSource();

            this.Worklogs.Clear();
            IEnumerable<Worklog> worklogs = Enumerable.Empty<Worklog>();

            try
            {
                worklogs = await this.Issue.InternalIssue.GetWorklogsAsync(this._worklogsCancellationSource.Token);
            }
            catch (OperationCanceledException)
            {
                // no-op
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("Unable to retrieve worklogs from issue.", ex);
            }

            foreach (var worklog in worklogs)
            {
                this.Worklogs.Add(worklog);
            }
        }

        public async Task LoadAttachmentsAsync()
        {
            this._attachmentsCancellationSource.Cancel();
            this._attachmentsCancellationSource = new CancellationTokenSource();

            this.Attachments.Clear();
            IEnumerable<Attachment> attachments = Enumerable.Empty<Attachment>();

            try
            {
                attachments = await this.Issue.InternalIssue.GetAttachmentsAsync(this._attachmentsCancellationSource.Token);
            }
            catch (OperationCanceledException)
            {
                // no-op
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("Unable to retrieve attachments from issue.", ex);
            }

            foreach (var attachment in attachments)
            {
                this.Attachments.Add(new AttachmentViewModel(attachment));
            }
        }

        private async Task TransitionIssueAsync()
        {
            var vm = new TransitionIssueDialogViewModel(this.Issue, this.Services);
            this.Services.WindowFactory.ShowDialog(vm);

            if (vm.Result.IsSucessful)
            {
                await this.RefreshIssueAsync();
            }
        }

        private async Task AddCommentAsync()
        {
            if (String.IsNullOrEmpty(this.NewComment))
            {
                return;
            }

            try
            {
                await this.Issue.InternalIssue.AddCommentAsync(this.NewComment);
                this.NewComment = String.Empty;
            }
            catch (OperationCanceledException)
            {
                // no-op
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("Failed to add comment to issue.", ex);
            }

            if (this.CommentsPager != null)
            {
                // refresh the comments.
                await this.CommentsPager.LoadItemsAsync();
            }
        }

        private void OnInitialized()
        {
            var args = new EventArgs();

            var handler = this.Initialized;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        private async Task RefreshIssueAsync()
        {
            this.IsIdle = false;

            Task issueRefreshTask = this._originalIssue.RefreshIssueAsync(CancellationToken.None);
            Task commentRefreshTask = this.CommentsPager != null ? this.CommentsPager.LoadItemsAsync() : Task.FromResult(true);

            try
            {
                await issueRefreshTask;
                this._editableIssue = _originalIssue.Clone();
                OnPropertyChanged("Issue");
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("There was an error retrieving the issue from server.", ex);
            }

            await commentRefreshTask;

            this.IsIdle = true;
        }

        private async Task SaveIssueAsync()
        {
            this.IsIdle = false;
            try
            {
                await this._originalIssue.UpdateIssueAsync(this._editableIssue, CancellationToken.None);
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("There was an error updating the issue.", ex);
            }

            this.IsIdle = true;
        }

        private void LoadComboBoxes()
        {
            if (this._editableIssue == null)
            {
                return;
            }

            var jira = this._editableIssue.Jira;
            foreach (var priority in jira.Priorities.GetPrioritiesAsync().Result)
            {
                this.Priorities.Add(priority.Name);
            }

            foreach (var type in jira.IssueTypes.GetIssueTypesAsync().Result)
            {
                this.Types.Add(type.Name);
            }

            foreach (var status in jira.Statuses.GetStatusesAsync().Result)
            {
                this.Statuses.Add(status.Name);
            }

            foreach (var resolution in jira.Resolutions.GetResolutionsAsync().Result)
            {
                this.Resolutions.Add(resolution.Name);
            }
        }

    }
}
