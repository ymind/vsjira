﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VSJira.Core.UI
{
    /// <summary>
    /// Interaction logic for ResolveIssueDialog.xaml
    /// </summary>
    public partial class TransitionIssueDialog : MetroWindow, ICloseableWindow
    {
        private readonly TransitionIssueDialogViewModel _viewModel;

        public TransitionIssueDialog(TransitionIssueDialogViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = _viewModel = viewModel;

            var theme = viewModel.Services.VisualStudioServices.GetConfigurationOptions().Theme;
            ThemeManager.Apply(this.Resources, theme);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await this._viewModel.LoadActionsAsync();
        }
    }
}
