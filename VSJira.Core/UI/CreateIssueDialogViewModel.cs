﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class CreateIssueDialogViewModel : ViewModelBase
    {
        private readonly VSJiraServices _services;
        private readonly Jira _jira;
        private readonly string _parentIssueKey;
        private string _summary;
        private string _description;
        private string _status;
        private IssueType _type;
        private IssuePriority _priority;
        private Project _project;
        private string _assignee;
        private string _reporter;
        private DateTime? _dueDate;

        public ObservableCollection<IssuePriority> Priorities { get; private set; }
        public ObservableCollection<IssueType> Types { get; private set; }
        public ObservableCollection<Project> Projects { get; private set; }
        public ObservableCollection<PostAction> PostActions { get; private set; }
        public DialogResult Result { get; private set; }
        public DelegateCommand<ICloseableWindow> CreateCommand { get; private set; }
        public DelegateCommand<ICloseableWindow> CancelCommand { get; private set; }

        public enum PostAction
        {
            None,
            VisualStudio,
            Browser
        }

        public class DialogResult
        {
            public PostAction PostAction { get; set; }
            public JiraIssueViewModel Issue { get; set; }
        }

        public CreateIssueDialogViewModel(Jira jira, string username, VSJiraServices services, string parentIssueKey = null)
        {
            this._services = services;
            this._jira = jira;
            this._parentIssueKey = parentIssueKey;

            this.Reporter = this.Assignee = username;
            this.Result = new DialogResult() { PostAction = PostAction.VisualStudio };
            this.Projects = new ObservableCollection<Project>();
            this.Types = new ObservableCollection<IssueType>();
            this.Priorities = new ObservableCollection<IssuePriority>();
            this.PostActions = new ObservableCollection<PostAction>()
            {
                PostAction.None,
                PostAction.Browser,
                PostAction.VisualStudio,
            };

            this.CancelCommand = new DelegateCommand<ICloseableWindow>((window) => window.Close());
            this.CreateCommand = new DelegateCommand<ICloseableWindow>(async (window) => await this.CreateIssueAsync(window));
        }

        public VSJiraServices Services
        {
            get { return _services; }
        }

        public PostAction SelectedPostAction
        {
            get
            {
                return this.Result.PostAction;
            }
            set
            {
                this.Result.PostAction = value;
                OnPropertyChanged("SelectedPostAction");
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        public string Summary
        {
            get { return _summary; }
            set
            {
                _summary = value;
                OnPropertyChanged("Summary");
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }

        public IssuePriority Priority
        {
            get { return _priority; }
            set
            {
                _priority = value;
                OnPropertyChanged("Priority");
            }
        }

        public IssueType Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        public DateTime? DueDate
        {
            get { return _dueDate; }
            set
            {
                _dueDate = value;
                OnPropertyChanged("DueDate");
            }
        }

        public string Reporter
        {
            get { return _reporter; }
            set
            {
                _reporter = value;
                OnPropertyChanged("Reporter");
            }
        }

        public string Assignee
        {
            get { return _assignee; }
            set
            {
                _assignee = value;
                OnPropertyChanged("Assignee");
            }
        }

        public Project Project
        {
            get { return _project; }
            set
            {
                _project = value;
                OnPropertyChanged("Project");
            }
        }

        private async Task CreateIssueAsync(ICloseableWindow window)
        {
            var issue = this._jira.CreateIssue(this.Project.Key, this._parentIssueKey);
            issue.Summary = this.Summary;
            issue.Priority = this.Priority;
            issue.Type = this.Type;
            issue.Assignee = this.Assignee;
            issue.Reporter = this.Reporter;
            issue.DueDate = this.DueDate;
            issue.Description = this.Description;

            this.Status = "Creating Issue...";

            try
            {
                var newIssueKey = await this._jira.Issues.CreateIssueAsync(issue);
                var newIssue = await this._jira.Issues.GetIssueAsync(newIssueKey);
                this.Result.Issue = new JiraIssueViewModel(newIssue, this.Services);
                this.Result.Issue.CopyIssueKeyCommand.Execute(null);
                window.Close();
            }
            catch (Exception ex)
            {
                this.Status = "Error: " + ex.Message;
            }
        }

        public async Task LoadComboboxesAsync()
        {
            var getPrioritiesTask = this._jira.Priorities.GetPrioritiesAsync();
            var getTypesTask = this._jira.IssueTypes.GetIssueTypesAsync();
            var getProjectsTask = this._jira.Projects.GetProjectsAsync();

            var priorities = Enumerable.Empty<IssuePriority>();
            var types = Enumerable.Empty<IssueType>();
            var projects = Enumerable.Empty<Project>();

            this.Status = "Retrieving priorties and types...";
            try
            {
                priorities = await getPrioritiesTask;
                types = await getTypesTask;
                projects = await getProjectsTask;
            }
            catch (OperationCanceledException)
            {
                // no-op
            }
            catch (Exception ex)
            {
                this.Status = "Error: " + ex.Message;
            }

            foreach (var priority in priorities)
            {
                this.Priorities.Add(priority);
            }

            foreach (var type in types)
            {
                this.Types.Add(type);
            }

            foreach (var project in projects)
            {
                this.Projects.Add(project);
            }

            this.Priority = this.Priorities.FirstOrDefault();
            this.Type = this.Types.FirstOrDefault();
            this.Project = this.Projects.FirstOrDefault();

            this.Status = "Ready.";
        }
    }
}
