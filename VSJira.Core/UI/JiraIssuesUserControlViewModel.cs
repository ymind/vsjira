﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraIssuesUserControlViewModel : JiraBackedViewModel
    {
        private JiraFilter _selectedFilter;
        private string _username;
        private CancellationTokenSource _filtersCancellationSource = new CancellationTokenSource();

        public JiraIssuesPagerViewModel IssuePager { get; private set; }
        public ObservableCollection<JiraFilter> Filters { get; private set; }
        public DelegateCommand OpenConnectDialogCommand { get; private set; }
        public DelegateCommand RefreshCommand { get; private set; }
        public DelegateCommand CreateIssueCommand { get; private set; }
        public DelegateCommand ShowOptionsPageCommand { get; private set; }

        public JiraIssuesUserControlViewModel(VSJiraServices services)
            : base(services)
        {
            this.IssuePager = new JiraIssuesPagerViewModel(services);
            this.Filters = new ObservableCollection<JiraFilter>();
            this.OpenConnectDialogCommand = new DelegateCommand(async () => await this.OpenConnectDialogAsync());
            this.RefreshCommand = new DelegateCommand(async () => await this.RefreshAsync());
            this.ShowOptionsPageCommand = new DelegateCommand(() => this.ShowOptionsPage());
            this.CreateIssueCommand = new DelegateCommand(async () => await this.CreateIssueAsync());
        }

        public JiraFilter SelectedFilter
        {
            get { return _selectedFilter; }
            set
            {
                _selectedFilter = value;
                OnPropertyChanged("SelectedFilter");
                OnPropertyChanged("IsFilterSelected");

                if (this.SelectedFilter != null)
                {
                    var maxIssuesPerRequest = this.Services.VisualStudioServices.GetConfigurationOptions().MaxIssuesPerRequest;
                    this.IssuePager.ResetItemsAsync(this.SelectedFilter.Jql, maxIssuesPerRequest);
                }
            }
        }

        public bool IsFilterSelected
        {
            get
            {
                return this.SelectedFilter != null;
            }
        }

        private Task<IEnumerable<JiraFilter>> GetFiltersAsync(CancellationToken token)
        {
            return this.Jira.Filters.GetFavouritesAsync(token);
        }

        private async Task CreateIssueAsync()
        {
            this._filtersCancellationSource.Cancel();

            var vm = new CreateIssueDialogViewModel(this.Jira, this._username, this.Services);
            this.Services.WindowFactory.ShowDialog(vm);

            if (vm.Result.Issue != null)
            {
                if (vm.Result.PostAction == CreateIssueDialogViewModel.PostAction.VisualStudio)
                {
                    vm.Result.Issue.OpenIssueToolWindowCommand.Execute(null);
                }
                else if (vm.Result.PostAction == CreateIssueDialogViewModel.PostAction.Browser)
                {
                    vm.Result.Issue.OpenIssueCommand.Execute(null);
                }

                await this.IssuePager.LoadItemsAsync();
            }
        }

        private async Task OpenConnectDialogAsync()
        {
            this._filtersCancellationSource.Cancel();

            var vm = new ConnectDialogViewModel(this.Services.JiraFactory, this.Services);
            this.Services.WindowFactory.ShowDialog(vm);

            if (vm.Result.Jira != null)
            {
                this.Jira = this.IssuePager.Jira = vm.Result.Jira;
                this._username = vm.Result.Username;
                await LoadFiltersComboBoxAsync();
            }
        }

        private async Task LoadFiltersComboBoxAsync(JiraFilter selectedFilter = null)
        {
            this._filtersCancellationSource = new CancellationTokenSource();
            this.Filters.Clear();

            IEnumerable<JiraFilter> filters = null;

            try
            {
                filters = (await GetFiltersAsync(this._filtersCancellationSource.Token)).ToArray();
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("There was an error retrieving the filters from server.", ex);
            }

            if (filters != null)
            {
                if (!filters.Any())
                {
                    Services.WindowFactory.ShowMessageBox("Warning: There are no filters marked as favorite in JIRA. This plugin only shows issues from your favorite filters.");
                }

                foreach (var filter in filters)
                {
                    this.Filters.Add(filter);
                }

                if (selectedFilter != null)
                {
                    // Re-select the previously selected filter
                    this.SelectedFilter = filters.FirstOrDefault(f => f.Id == selectedFilter.Id);
                }
                else
                {
                    // Automatically select the first filter.
                    this.SelectedFilter = filters.FirstOrDefault();
                }
            }
        }

        private void ShowOptionsPage()
        {
            this.Services.VisualStudioServices.ShowOptionsPage();
        }

        private async Task RefreshAsync()
        {
            if (this.IsConnected)
            {
                this.IssuePager.CancelOperation();
                this._filtersCancellationSource.Cancel();
                await LoadFiltersComboBoxAsync(this.SelectedFilter);
            }
        }
    }
}