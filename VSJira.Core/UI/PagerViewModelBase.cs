﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VSJira.Core.UI
{
    public abstract class PagerViewModelBase<T> : JiraBackedViewModel
    {
        protected int _itemsPerPage;
        protected int _totalItems;
        protected int _currentItemIndex;

        private CancellationTokenSource _cancellationSource;
        public DelegateCommand FirstCommand { get; private set; }
        public DelegateCommand PreviousCommand { get; private set; }
        public DelegateCommand NextCommand { get; private set; }
        public DelegateCommand LastCommand { get; private set; }

        public PagerViewModelBase(VSJiraServices services)
            : base(services)
        {
            this._cancellationSource = new CancellationTokenSource();

            InitializeCommands();
        }

        public int ItemsPerPage
        {
            get { return _itemsPerPage; }
        }

        public int TotalItems
        {
            get { return _totalItems; }
        }

        public int Start
        {
            get { return _currentItemIndex + 1; }
        }

        public int End
        {
            get { return _currentItemIndex + _itemsPerPage < _totalItems ? _currentItemIndex + _itemsPerPage : _totalItems; }
        }

        public void CancelOperation()
        {
            this._cancellationSource.Cancel();
            this.OnOperationCancelled();
        }

        public async Task LoadItemsAsync()
        {
            this.CancelOperation();

            this.OnLoadingItems();

            this._cancellationSource = new CancellationTokenSource();

            // refresh UI of expected results before issuing the request.
            UpdatePageCounts();

            IPagedQueryResult<T> pagedResult = null;

            try
            {
                pagedResult = await this.GetItemsAsync(this._itemsPerPage, this._currentItemIndex, _cancellationSource.Token);
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox($"There was an error retrieving the items of type {typeof(T).ToString()} from server.", ex);
            }

            if (pagedResult != null)
            {
                this._totalItems = pagedResult.TotalItems;
                UpdatePageCounts();
            }
        }

        protected async Task ResetItemsAsync(int maxIssuesPerRequest)
        {
            this.CancelOperation();

            OnResettingItems();

            this._currentItemIndex = 0;
            this._itemsPerPage = maxIssuesPerRequest;

            await this.LoadItemsAsync();
        }

        protected abstract Task<IPagedQueryResult<T>> GetItemsAsync(int itemsPerPage, int startAt, CancellationToken token);

        protected virtual void OnResettingItems()
        {
            // no-op
        }

        protected virtual void OnLoadingItems()
        {
            // no-op
        }

        protected virtual void OnOperationCancelled()
        {
            // no-op
        }

        private void UpdatePageCounts()
        {
            OnPropertyChanged("TotalItems");
            OnPropertyChanged("Start");
            OnPropertyChanged("End");

            CommandManager.InvalidateRequerySuggested();
        }

        private void InitializeCommands()
        {
            this.FirstCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex = 0;
                    await this.LoadItemsAsync();
                },
                () => _currentItemIndex - _itemsPerPage >= 0 ? true : false);

            this.PreviousCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex -= _itemsPerPage;
                    await this.LoadItemsAsync();
                },
                () => _currentItemIndex - _itemsPerPage >= 0 ? true : false);

            this.NextCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex += _itemsPerPage;
                    await this.LoadItemsAsync();
                },
                () => _currentItemIndex + _itemsPerPage < _totalItems ? true : false);

            this.LastCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex = (_totalItems / _itemsPerPage - 1) * _itemsPerPage;
                    _currentItemIndex += _totalItems % _itemsPerPage == 0 ? 0 : _itemsPerPage;
                    await this.LoadItemsAsync();
                },
                () => _currentItemIndex + _itemsPerPage < _totalItems ? true : false);
        }
    }
}
