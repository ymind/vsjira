﻿using Atlassian.Jira;
using System;
using System.IO;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class TestWebClient : IWebClient
    {
        public void AddQueryString(string key, string value)
        {
        }

        public void Download(string url, string fileName)
        {
            File.WriteAllText(fileName, url);
        }

        public Task DownloadAsync(string url, string fileName)
        {
            Download(url, fileName);
            return Task.FromResult(fileName);
        }
    }
}
